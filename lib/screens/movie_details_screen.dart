import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:jenz/bloc/movie_bloc.dart';
import 'package:jenz/bloc/network_bloc.dart';
import 'package:jenz/constants/endpoint_uris.dart';
import 'package:jenz/constants/jenz_icons.dart';
import 'package:jenz/models/models.dart';
import 'package:jenz/widgets/genre_card.dart';
import 'package:jenz/widgets/rating_widget.dart';
import 'package:collection/collection.dart';

class MovieDetailsScreen extends StatelessWidget {
  final Movie movie;
  final List<Genre> movieGenres;
  final bool isFavorite;

  const MovieDetailsScreen(this.movie, this.movieGenres, {this.isFavorite = false, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<NetworkBloc, NetworkState>(
        listenWhen: (NetworkState oldState, NetworkState newState) {
          if (newState.internetConnectionStatus == InternetConnectionStatus.error ||
              (oldState.internetConnectionStatus != InternetConnectionStatus.connected &&
                  newState.internetConnectionStatus == InternetConnectionStatus.connected)) {
            return true;
          } else {
            return false;
          }
        },
        listener: (BuildContext context, NetworkState networkState) {
          if (networkState.internetConnectionStatus == InternetConnectionStatus.error) {
            WidgetsBinding.instance?.addPostFrameCallback((_) {
              ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(content: Text("You are not connected to the internet.")),
              );
            });
          } else {
            BlocProvider.of<MovieBloc>(context).add(FetchMovies());
          }
        },
        child: BlocBuilder<MovieBloc, MovieState>(
          builder: (BuildContext context, MovieState state) {
            if (state.movieDetails == null) {
              return const SafeArea(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            }
            return SafeArea(
              child: SizedBox(
                height: MediaQuery.of(context).size.height,
                child: Stack(
                  children: <Widget>[
                    Image.network(
                      EndpointUris.imagesHostName + (state.movieDetails!.backdropPath ?? ""),
                      height: 335,
                      width: double.infinity,
                      frameBuilder: (BuildContext context, Widget child, int? frame, bool wasSynchronouslyLoaded) {
                        return child;
                      },
                      loadingBuilder: (BuildContext context, Widget child, ImageChunkEvent? loadingProgress) {
                        if (loadingProgress == null) {
                          return child;
                        } else {
                          return const Center(
                            child: CircularProgressIndicator(),
                          );
                        }
                      },
                      errorBuilder: (BuildContext context, _, __) {
                        return Container(
                          color: Theme.of(context).cardColor,
                          child: Center(
                            child: SvgPicture.asset("assets/images/logo.svg"),
                          ),
                        );
                      },
                      fit: BoxFit.fill,
                    ),
                    _buildMainContent(context, state.movieDetails!, state.favoriteMovies),
                    Positioned(
                      top: 0,
                      left: 0,
                      child: IconButton(
                        icon: const Icon(
                          JenzIcons.arrow_left,
                          size: 12,
                          color: Color(0xFFE4ECEF),
                        ),
                        onPressed: () => Navigator.of(context).pop(),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  Widget _buildMainContent(BuildContext context, MovieDetails movieDetails, List<Movie> favoriteMovies) {
    return Positioned(
      top: 315,
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.fromLTRB(20, 28, 20, 0),
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20)),
          color: Theme.of(context).backgroundColor,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: Text(
                    movieDetails.title ?? "",
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                ),
                const SizedBox(width: 16),
                GestureDetector(
                  onTap: () => BlocProvider.of<MovieBloc>(context).add(AddOrRemoveFavoriteMovie(movie)),
                  child: Icon(
                    favoriteMovies.firstWhereOrNull((Movie m) => m.id == movieDetails.id) != null
                        ? JenzIcons.favorite_checked
                        : JenzIcons.favorite_unchecked,
                    size: 18,
                    color: favoriteMovies.firstWhereOrNull((Movie m) => m.id == movieDetails.id) != null
                        ? Theme.of(context).primaryColor
                        : const Color(0xFFE4ECEF),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 10),
            RatingWidget(movieDetails.voteAverage ?? 0.0),
            const SizedBox(height: 18),
            _generateGenreCardWidgets(),
            const SizedBox(height: 40),
            Text("Description", style: Theme.of(context).textTheme.titleSmall),
            const SizedBox(height: 8),
            SingleChildScrollView(
              child: Text(
                movieDetails.overview ?? "",
                style: Theme.of(context).textTheme.bodyMedium,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _generateGenreCardWidgets() {
    List<Widget> children = <Widget>[];
    for (Genre genre in movieGenres) {
      children.addAll(<Widget>[
        GenreCard(genre),
        const SizedBox(width: 4),
      ]);
    }
    return SingleChildScrollView(
      primary: false,
      scrollDirection: Axis.horizontal,
      child: Row(
        children: children,
      ),
    );
  }
}
