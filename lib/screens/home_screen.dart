import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:jenz/bloc/movie_bloc.dart';
import 'package:jenz/bloc/network_bloc.dart';
import 'package:jenz/constants/jenz_icons.dart';
import 'package:jenz/models/models.dart';
import 'package:jenz/screens/movie_details_screen.dart';
import 'package:jenz/widgets/jenz_bottom_navigation_bar_item.dart';
import 'package:jenz/widgets/movie_item.dart';
import 'package:collection/collection.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

enum _SelectedTab { popular, favorites }

class _HomeScreenState extends State<HomeScreen> {
  _SelectedTab selectedTab = _SelectedTab.popular;
  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_scrollEventListener);
  }

  @override
  void dispose() {
    _scrollController
      ..removeListener(_scrollEventListener)
      ..dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        appBar: _buildAppBar(),
        body: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              const SizedBox(height: 32),
              Text(
                selectedTab == _SelectedTab.popular ? "Popular" : "Favourites",
                style: Theme.of(context).textTheme.titleLarge,
              ),
              const SizedBox(height: 10),
              Expanded(child: _buildMovieList()),
            ],
          ),
        ),
        bottomNavigationBar: Container(
          height: 60,
          padding: const EdgeInsets.symmetric(horizontal: 60),
          color: Theme.of(context).bottomNavigationBarTheme.backgroundColor,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              JenzBottomNavigationBarItem(
                icon: JenzIcons.movies,
                label: "Movies",
                isSelected: selectedTab == _SelectedTab.popular,
                onTap: () {
                  setState(() {
                    if (selectedTab != _SelectedTab.popular) {
                      selectedTab = _SelectedTab.popular;
                    }
                  });
                },
              ),
              const SizedBox(width: 40),
              JenzBottomNavigationBarItem(
                icon: JenzIcons.favorites,
                label: "Favourites",
                isSelected: selectedTab == _SelectedTab.favorites,
                onTap: () {
                  setState(() {
                    if (selectedTab != _SelectedTab.favorites) {
                      selectedTab = _SelectedTab.favorites;
                    }
                  });
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildMovieList() {
    return BlocListener<NetworkBloc, NetworkState>(
      listenWhen: (NetworkState oldState, NetworkState newState) {
        if (newState.internetConnectionStatus == InternetConnectionStatus.error ||
            (oldState.internetConnectionStatus != InternetConnectionStatus.connected &&
                newState.internetConnectionStatus == InternetConnectionStatus.connected)) {
          return true;
        } else {
          return false;
        }
      },
      listener: (BuildContext context, NetworkState networkState) {
        if (networkState.internetConnectionStatus == InternetConnectionStatus.error) {
          WidgetsBinding.instance?.addPostFrameCallback((_) {
            ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(content: Text("You are not connected to the internet.")),
            );
          });
        } else {
          BlocProvider.of<MovieBloc>(context).add(FetchMovies());
        }
      },
      child: BlocBuilder<MovieBloc, MovieState>(
        builder: (BuildContext context, MovieState state) {
          switch (state.movieFetchStatus) {
            case MovieFetchStatus.loading:
              return const Center(child: CircularProgressIndicator());
            case MovieFetchStatus.error:
              return Center(
                child: Text(
                  "Oops, something went wrong!\nTry again later",
                  style: Theme.of(context).textTheme.labelMedium,
                  textAlign: TextAlign.center,
                ),
              );
            case MovieFetchStatus.fetched:
              return selectedTab == _SelectedTab.popular
                  ? _buildPopularMoviesList(state.movies, state.favoriteMovies, state.genres)
                  : _buildFavoriteMoviesList(state.favoriteMovies, state.genres);
            default:
              return const SizedBox.shrink();
          }
        },
      ),
    );
  }

  PreferredSizeWidget _buildAppBar() {
    return AppBar(
      leading: Padding(
        padding: const EdgeInsets.only(top: 15, left: 20),
        child: SvgPicture.asset("assets/images/logo.svg"),
      ),
      elevation: 0,
      backgroundColor: Theme.of(context).backgroundColor,
    );
  }

  Widget _buildPopularMoviesList(List<Movie> popularMovies, List<Movie> favoriteMovies, List<Genre> genres) =>
      ListView.builder(
        controller: _scrollController,
        itemCount: popularMovies.length + 1,
        itemBuilder: (BuildContext context, int index) {
          if (index == popularMovies.length) {
            return const Center(
              child: SizedBox(
                height: 24,
                width: 24,
                child: CircularProgressIndicator(),
              ),
            );
          }
          return MovieItem(
              popularMovies[index],
              _getMovieGenres(
                popularMovies[index].genreIds ?? <int>[],
                genres,
              ),
              isFavorite: favoriteMovies.firstWhereOrNull((Movie movie) => movie.id == popularMovies[index].id) != null,
              onTap: () {
            BlocProvider.of<MovieBloc>(context).add(GetMovieDetails(popularMovies[index]));
            Navigator.of(context).push(
              _buildMovieDetailsRoute(
                popularMovies[index],
                _getMovieGenres(
                  popularMovies[index].genreIds ?? <int>[],
                  genres,
                ),
              ),
            );
          });
        },
      );

  Widget _buildFavoriteMoviesList(List<Movie> favoriteMovies, List<Genre> genres) {
    if (favoriteMovies.isEmpty) {
      return Center(
        child: RichText(
          text: TextSpan(
            style: Theme.of(context).textTheme.labelMedium,
            children: const <InlineSpan>[
              TextSpan(text: "No favourite movies found, please add one by tapping "),
              WidgetSpan(child: Icon(JenzIcons.favorite_unchecked, size: 12, color: Color(0xFFE4ECEF))),
              TextSpan(text: " icon"),
            ],
          ),
        ),
      );
    } else {
      return ListView.builder(
        itemCount: favoriteMovies.length,
        itemBuilder: (BuildContext context, int index) {
          return MovieItem(
            favoriteMovies[index],
            _getMovieGenres(
              favoriteMovies[index].genreIds ?? <int>[],
              genres,
            ),
            isFavorite: true,
            onTap: () {
              BlocProvider.of<MovieBloc>(context).add(GetMovieDetails(favoriteMovies[index]));
              Navigator.of(context).push(
                _buildMovieDetailsRoute(
                  favoriteMovies[index],
                  _getMovieGenres(
                    favoriteMovies[index].genreIds ?? <int>[],
                    genres,
                  ),
                ),
              );
            },
          );
        },
      );
    }
  }

  List<Genre> _getMovieGenres(List<int> movieGenreIds, List<Genre> genres) =>
      genres.where((Genre genre) => movieGenreIds.contains(genre.id)).toList();

  void _scrollEventListener() {
    if (_scrollController.offset >= (_scrollController.position.maxScrollExtent * 0.95)) {
      BlocProvider.of<MovieBloc>(context).add(LoadMoreMovies());
    }
  }

  Route _buildMovieDetailsRoute(
    Movie movie,
    List<Genre> genres,
  ) {
    return PageRouteBuilder(
      pageBuilder: (BuildContext context, Animation<double> primaryAnimation, Animation<double> secondaryAnimation) {
        return MovieDetailsScreen(movie, genres);
      },
      transitionsBuilder: (BuildContext context, Animation<double> primaryAnimation,
          Animation<double> secondaryAnimation, Widget child) {
        const Offset startingOffset = Offset(1, 0);
        const Offset endingOffset = Offset(0, 0);
        final Tween<Offset> tween = Tween(begin: startingOffset, end: endingOffset);

        return SlideTransition(position: primaryAnimation.drive(tween), child: child);
      },
    );
  }
}
