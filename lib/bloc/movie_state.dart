part of 'movie_bloc.dart';

enum MovieFetchStatus {
  loading,
  fetched,
  error,
}

class MovieState extends Equatable {
  final MovieFetchStatus movieFetchStatus;
  final int page;
  final int? totalPages;
  final String language;
  final List<Movie> movies;
  final List<Movie> favoriteMovies;
  final List<Genre> genres;
  final MovieDetails? movieDetails;
  final bool isLoadMoreCalled;

  const MovieState({
    this.movieFetchStatus = MovieFetchStatus.loading,
    this.page = 1,
    this.totalPages,
    this.language = "en_US",
    this.movies = const <Movie>[],
    this.favoriteMovies = const <Movie>[],
    this.genres = const <Genre>[],
    this.movieDetails,
    this.isLoadMoreCalled = false,
  });

  @override
  List<Object?> get props => <Object?>[
        movieFetchStatus,
        page,
        totalPages,
        language,
        movies,
        favoriteMovies,
        genres,
        movieDetails,
        isLoadMoreCalled,
      ];

  MovieState copyWith({
    MovieFetchStatus? movieFetchStatus,
    int? page,
    int? totalPages,
    String? language,
    List<Movie>? movies,
    List<Movie>? favoriteMovies,
    List<Genre>? genres,
    ValueGetter<MovieDetails?>? movieDetails,
    bool? isLoadMoreCalled,
  }) {
    return MovieState(
      movieFetchStatus: movieFetchStatus ?? this.movieFetchStatus,
      page: page ?? this.page,
      totalPages: totalPages ?? this.totalPages,
      language: language ?? this.language,
      movies: movies ?? this.movies,
      favoriteMovies: favoriteMovies ?? this.favoriteMovies,
      genres: genres ?? this.genres,
      movieDetails: movieDetails != null ? movieDetails() : this.movieDetails,
      isLoadMoreCalled: isLoadMoreCalled ?? this.isLoadMoreCalled,
    );
  }
}
