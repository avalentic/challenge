import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:hive/hive.dart';
import 'package:jenz/constants/endpoint_uris.dart';
import 'package:jenz/database/database_helper.dart';
import 'package:jenz/models/models.dart';
import 'package:jenz/tools/endpoint_url_helper.dart';
import 'package:collection/collection.dart';

part 'movie_event.dart';

part 'movie_state.dart';

class MovieBloc extends Bloc<MovieEvent, MovieState> {
  final Dio client;

  MovieBloc(
    MovieState initialState, {
    required this.client,
  }) : super(initialState) {
    on<FetchMovies>(_fetchMoviesAdvanced);
    on<LoadMoreMovies>(_loadMore);
    on<AddOrRemoveFavoriteMovie>(_addOrRemoveFavoriteMovie);
    on<GetMovieDetails>(_openMovieDetails);
  }

  Future<void> _fetchMoviesSimple(FetchMovies event, Emitter<MovieState> emit) async {
    if (state.totalPages != null && state.page > state.totalPages!) {
      return;
    }
    try {
      emit(state.copyWith(movieFetchStatus: MovieFetchStatus.loading));

      String? apiKey = dotenv.env["API_KEY"];
      String moviesUrl =
          EndpointUris.getPopularMovies + "?api_key=$apiKey&language=${state.language}&page=${state.page}";
      String genresUrl = EndpointUris.getGenres + "?api_key=$apiKey";
      List<dynamic> response = await Future.wait(<Future<dynamic>>[
        client.get(moviesUrl),
        client.get(genresUrl),
      ]);

      Iterable movieResults = response.first.data["results"];
      Iterable genreResults = response.last.data["genres"];
      final List<Movie> movies = List<Movie>.from(movieResults.map((dynamic result) => Movie.fromJson(result)));
      final List<Genre> genres = List<Genre>.from(genreResults.map((dynamic result) => Genre.fromJson(result)));

      List<Box<dynamic>> boxes = await Future.wait(<Future<Box<dynamic>>>[
        DatabaseHelper().moviesBox,
        DatabaseHelper().favoriteMoviesBox,
        DatabaseHelper().genresBox,
      ]);

      _cacheData(
        movieBox: boxes.first as Box<Movie>,
        movies: movies,
        genreBox: boxes.last as Box<Genre>,
        genres: genres,
      );

      return emit(state.copyWith(
        movies: movies,
        favoriteMovies: boxes[1].values.toList() as List<Movie>,
        genres: genres,
        page: state.page + 1,
        totalPages: response.first.data["total_pages"],
        movieFetchStatus: MovieFetchStatus.fetched,
      ));
    } catch (_) {
      emit(state.copyWith(movieFetchStatus: MovieFetchStatus.error));

      List<Box<dynamic>> boxes = await Future.wait(<Future<Box<dynamic>>>[
        DatabaseHelper().moviesBox,
        DatabaseHelper().favoriteMoviesBox,
        DatabaseHelper().genresBox,
      ]);
      return emit(state.copyWith(
        movies: boxes.first.values.toList() as List<Movie>? ?? <Movie>[],
        favoriteMovies: boxes[1].values.toList() as List<Movie>? ?? <Movie>[],
        genres: boxes.last.values.toList() as List<Genre>? ?? <Genre>[],
        page: 1,
        totalPages: 0,
        movieFetchStatus: MovieFetchStatus.fetched,
      ));
    }
  }

  Future<void> _fetchMoviesAdvanced(FetchMovies event, Emitter<MovieState> emit) async {
    if (state.totalPages != null && state.page > state.totalPages!) {
      return;
    }
    try {
      emit(state.copyWith(movieFetchStatus: MovieFetchStatus.loading));

      String moviesUrl = EndpointUris.getPopularMovies + "?language=${state.language}&page=${state.page}";
      String genresUrl = EndpointUris.getGenres;
      List<dynamic> response = await Future.wait(<Future<dynamic>>[
        client.get(moviesUrl),
        client.get(genresUrl),
      ]);

      Iterable movieResults = response.first.data["results"];
      Iterable genreResults = response.last.data["genres"];
      final List<Movie> movies = List<Movie>.from(movieResults.map((dynamic result) => Movie.fromJson(result)));
      final List<Genre> genres = List<Genre>.from(genreResults.map((dynamic result) => Genre.fromJson(result)));

      List<Box<dynamic>> boxes = await Future.wait(<Future<Box<dynamic>>>[
        DatabaseHelper().moviesBox,
        DatabaseHelper().favoriteMoviesBox,
        DatabaseHelper().genresBox,
      ]);

      _cacheData(
        movieBox: boxes.first as Box<Movie>,
        movies: movies,
        genreBox: boxes.last as Box<Genre>,
        genres: genres,
      );

      return emit(state.copyWith(
        movies: movies,
        favoriteMovies: boxes[1].values.toList() as List<Movie>,
        genres: genres,
        page: state.page + 1,
        totalPages: response.first.data["total_pages"],
        movieFetchStatus: MovieFetchStatus.fetched,
      ));
    } catch (_) {
      emit(state.copyWith(movieFetchStatus: MovieFetchStatus.error));

      List<Box<dynamic>> boxes = await Future.wait(<Future<Box<dynamic>>>[
        DatabaseHelper().moviesBox,
        DatabaseHelper().genresBox,
      ]);
      return emit(state.copyWith(
        movies: boxes.first.values.toList() as List<Movie>? ?? <Movie>[],
        genres: boxes.last.values.toList() as List<Genre>? ?? <Genre>[],
        page: 1,
        totalPages: 0,
        movieFetchStatus: MovieFetchStatus.fetched,
      ));
    }
  }

  Future<void> _loadMore(LoadMoreMovies event, Emitter<MovieState> emit) async {
    if (state.totalPages != null && state.page > state.totalPages! || state.isLoadMoreCalled) {
      return;
    }
    try {
      emit(state.copyWith(isLoadMoreCalled: true));

      String moviesUrl = EndpointUris.getPopularMovies + "?language=${state.language}&page=${state.page}";
      Response<dynamic> response = await client.get(moviesUrl);

      Iterable movieResults = response.data["results"];
      final List<Movie> movies = List<Movie>.from(movieResults.map((dynamic result) => Movie.fromJson(result)));
      movies.insertAll(0, state.movies);

      Box<Movie> moviesBox = await DatabaseHelper().moviesBox;
      _cacheData(movieBox: moviesBox, movies: movies);

      return emit(state.copyWith(
        movies: movies,
        page: state.page + 1,
        totalPages: response.data["total_pages"],
        movieFetchStatus: MovieFetchStatus.fetched,
        isLoadMoreCalled: false,
      ));
    } catch (_) {
      emit(state.copyWith(movieFetchStatus: MovieFetchStatus.error));
    }
  }

  Future<void> _addOrRemoveFavoriteMovie(AddOrRemoveFavoriteMovie event, Emitter<MovieState> emit) async {
    try {
      Box<Movie> favoriteMoviesBox = await DatabaseHelper().favoriteMoviesBox;
      Movie? movie = favoriteMoviesBox.values.toList().firstWhereOrNull((Movie movie) => movie.id == event.movie.id);
      if (movie != null) {
        favoriteMoviesBox.delete(movie.id);
      } else {
        favoriteMoviesBox.put(event.movie.id ?? -1, event.movie.copyWith());
      }

      return emit(state.copyWith(
        favoriteMovies: favoriteMoviesBox.values.toList(),
        movieFetchStatus: MovieFetchStatus.fetched,
      ));
    } catch (_) {
      emit(state.copyWith(movieFetchStatus: MovieFetchStatus.error));
    }
  }

  Future<void> _openMovieDetails(GetMovieDetails event, Emitter<MovieState> emit) async {
    try {
      emit(state.copyWith(movieDetails: () => null));

      String url = EndpointUrlHelper.getUrlWithParams(EndpointUris.getMovieDetails, <String, String>{
        "id": event.movie.id?.toString() ?? "-1",
      });

      Response<dynamic> response = await client.get(url);

      MovieDetails movieDetails = MovieDetails.fromJson(response.data);

      return emit(state.copyWith(
        movieDetails: () => movieDetails,
        movieFetchStatus: MovieFetchStatus.fetched,
      ));
    } catch (_) {
      emit(state.copyWith(movieDetails: () => null));
    }
  }

  void _cacheData({Box<Movie>? movieBox, List<Movie>? movies, Box<Genre>? genreBox, List<Genre>? genres}) {
    if ((movies?.isNotEmpty ?? false) && movieBox != null) {
      final Map<int, Movie> moviesMap = <int, Movie>{};
      for (Movie movie in movies!) {
        moviesMap[movie.id ?? -1] = movie;
      }
      movieBox.putAll(moviesMap);
    }

    if ((genres?.isNotEmpty ?? false) && genreBox != null) {
      final Map<int, Genre> genresMap = <int, Genre>{};
      for (Genre genre in genres!) {
        genresMap[genre.id ?? -1] = genre;
      }
      genreBox.putAll(genresMap);
    }
  }
}
