part of 'movie_bloc.dart';

class MovieEvent extends Equatable {
  @override
  List<Object?> get props => const <Object?>[];
}

class FetchMovies extends MovieEvent {}

class LoadMoreMovies extends MovieEvent {}

class FetchFavoriteMovies extends MovieEvent {}

class GetMovieDetails extends MovieEvent {
  final Movie movie;

  GetMovieDetails(this.movie);
}

class AddOrRemoveFavoriteMovie extends MovieEvent {
  final Movie movie;

  AddOrRemoveFavoriteMovie(this.movie);
}
