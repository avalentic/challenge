part of 'network_bloc.dart';

class NetworkEvent extends Equatable {
  @override
  List<Object?> get props => const <Object?>[];
}

class NetworkObserve extends NetworkEvent {}

class NetworkNotify extends NetworkEvent {
  final ConnectivityResult connectivityResult;

  NetworkNotify(this.connectivityResult);
}
