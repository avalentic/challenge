import 'package:bloc/bloc.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jenz/tools/network_helper.dart';

part 'network_event.dart';

part 'network_state.dart';

class NetworkBloc extends Bloc<NetworkEvent, NetworkState> {
  static final NetworkBloc _instance = NetworkBloc._();

  NetworkBloc._() : super(const NetworkState()) {
    on<NetworkNotify>(_networkNotify);
    on<NetworkObserve>(_networkObserve);
  }

  factory NetworkBloc() => _instance;

  void _networkNotify(NetworkNotify event, Emitter<NetworkState> emit) {
    try {
      if (event.connectivityResult == ConnectivityResult.none) {
        return emit(state.copyWith(internetConnectionStatus: InternetConnectionStatus.error));
      } else {
        return emit(state.copyWith(internetConnectionStatus: InternetConnectionStatus.connected));
      }
    } catch (_) {
      return emit(state.copyWith(internetConnectionStatus: InternetConnectionStatus.unknown));
    }
  }

  void _networkObserve(NetworkObserve event, Emitter<NetworkState> emit) {
    return NetworkHelper.observeNetwork();
  }
}
