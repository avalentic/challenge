part of 'network_bloc.dart';

enum InternetConnectionStatus {
  unknown,
  connected,
  error,
}

class NetworkState extends Equatable {
  final InternetConnectionStatus internetConnectionStatus;

  @override
  List<Object?> get props => <Object?>[internetConnectionStatus];

//<editor-fold desc="Data Methods">

  const NetworkState({
    this.internetConnectionStatus = InternetConnectionStatus.unknown,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is NetworkState &&
          runtimeType == other.runtimeType &&
          internetConnectionStatus == other.internetConnectionStatus);

  @override
  int get hashCode => internetConnectionStatus.hashCode;

  @override
  String toString() {
    return 'NetworkState{' + ' internetConnectionStatus: $internetConnectionStatus,' + '}';
  }

  NetworkState copyWith({
    InternetConnectionStatus? internetConnectionStatus,
  }) {
    return NetworkState(
      internetConnectionStatus: internetConnectionStatus ?? this.internetConnectionStatus,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'internetConnectionStatus': internetConnectionStatus,
    };
  }

  factory NetworkState.fromMap(Map<String, dynamic> map) {
    return NetworkState(
      internetConnectionStatus: map['internetConnectionStatus'] as InternetConnectionStatus,
    );
  }

//</editor-fold>
}
