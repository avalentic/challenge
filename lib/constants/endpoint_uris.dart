class EndpointUris {
  static const imagesHostName = "https://image.tmdb.org/t/p/w500/";

  static const getPopularMovies = "movie/popular";
  static const getMovieDetails = "movie/{id}";
  static const getGenres = "genre/movie/list";
}
