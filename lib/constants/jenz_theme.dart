import 'package:flutter/material.dart';

class JenzTheme {
  static const ratingStarIconColor = Color(0xFFF2CF16);

  static getThemeData() => ThemeData(
        primaryColor: const Color(0xFFEC9B3E),
        cardColor: const Color.fromRGBO(236, 155, 62, 0.2),
        backgroundColor: const Color(0xFF0E1324),
        canvasColor: const Color(0xFF0E1324),
        bottomNavigationBarTheme: _bottomNavigationBarTheme(),
        textTheme: _getTextTheme(),
        progressIndicatorTheme: const ProgressIndicatorThemeData(color: Color(0xFFEC9B3E)),
      );

  static _bottomNavigationBarTheme() => const BottomNavigationBarThemeData(
        backgroundColor: Color(0xFF010510),
        selectedIconTheme: IconThemeData(
          color: Color(0xFFEC9B3E),
          opacity: 1,
          size: 18,
        ),
        unselectedIconTheme: IconThemeData(
          color: Color(0xFFFFFFFF),
          opacity: 1,
          size: 18,
        ),
        selectedItemColor: Color(0xFFEC9B3E),
        unselectedItemColor: Color(0xFFFFFFFF),
        selectedLabelStyle: TextStyle(
          fontFamily: "SF Pro Display",
          fontSize: 12,
          fontWeight: FontWeight.w600,
          color: Color(0xFFEC9B3E),
        ),
        unselectedLabelStyle: TextStyle(
          fontFamily: "SF Pro Display",
          fontSize: 12,
          fontWeight: FontWeight.w600,
          color: Color(0xFFE4ECEF),
        ),
        showSelectedLabels: true,
        showUnselectedLabels: true,
        type: BottomNavigationBarType.fixed,
        enableFeedback: true,
      );

  static _getTextTheme() => const TextTheme(
        titleLarge: TextStyle(
          fontFamily: "SF Pro Display",
          fontSize: 22,
          fontWeight: FontWeight.w700,
          color: Color(0xFFE4ECEF),
        ),
        titleMedium: TextStyle(
          fontFamily: "SF Pro Display",
          fontSize: 20,
          fontWeight: FontWeight.w700,
          color: Color(0xFFE4ECEF),
        ),
        titleSmall: TextStyle(
          fontFamily: "SF Pro Display",
          fontSize: 15,
          fontWeight: FontWeight.w700,
          color: Color(0xFFE4ECEF),
        ),
        labelMedium: TextStyle(
          fontFamily: "SF Pro Display",
          fontSize: 12,
          fontWeight: FontWeight.w400,
          color: Color(0xFFE4ECEF),
        ),
        labelSmall: TextStyle(
          fontFamily: "SF Pro",
          fontSize: 11,
          fontWeight: FontWeight.w400,
          color: Color(0xFFE4ECEF),
        ),
        bodyMedium: TextStyle(
          fontFamily: "SF Pro Display",
          fontSize: 13,
          fontWeight: FontWeight.w300,
          color: Color(0xFFE4ECEF),
        ),
      );
}
