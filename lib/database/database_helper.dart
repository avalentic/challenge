import 'package:hive/hive.dart';
import 'package:jenz/models/models.dart';

class DatabaseHelper {
  static const _moviesBoxName = "movies";
  static const _movieGenresBoxName = "genres";
  static const _favoriteMoviesBoxName = "favoriteMovies";

  static final DatabaseHelper _instance = DatabaseHelper._internal();

  DatabaseHelper._internal();

  factory DatabaseHelper() {
    return _instance;
  }

  static Box<Movie>? _moviesBox;
  Future<Box<Movie>> get moviesBox async {
    if(_moviesBox != null) {
      return _moviesBox!;
    } else {
      _moviesBox = await Hive.openBox<Movie>(_moviesBoxName);
      return _moviesBox!;
    }
  }

  static Box<Genre>? _genresBox;
  Future<Box<Genre>> get genresBox async {
    if(_genresBox != null) {
      return _genresBox!;
    } else {
      _genresBox = await Hive.openBox<Genre>(_movieGenresBoxName);
      return _genresBox!;
    }
  }

  static Box<Movie>? _favoriteMoviesBox;
  Future<Box<Movie>> get favoriteMoviesBox async {
    if(_favoriteMoviesBox != null) {
      return _favoriteMoviesBox!;
    } else {
      _favoriteMoviesBox = await Hive.openBox(_favoriteMoviesBoxName);
      return _favoriteMoviesBox!;
    }
  }
}
