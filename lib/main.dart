import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:jenz/bloc/movie_bloc.dart';
import 'package:jenz/bloc/network_bloc.dart';
import 'package:jenz/constants/jenz_theme.dart';
import 'package:jenz/jenz_bloc_observer.dart';
import 'package:jenz/models/models.dart';
import 'package:jenz/screens/home_screen.dart';

import 'http_client/http_client_factory.dart';

void main() async {
  Bloc.observer = JenzBlocObserver();
  await Future.wait(
    <Future<dynamic>>[
      dotenv.load(fileName: ".env"),
      Hive.initFlutter(),
    ],
  );
  Hive.registerAdapter(MovieAdapter());
  Hive.registerAdapter(GenreAdapter());
  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider<MovieBloc>(
          create: (_) => MovieBloc(const MovieState(), client: HttpClientFactory().client)..add(FetchMovies()),
        ),
        BlocProvider<NetworkBloc>(create: (_) => NetworkBloc()..add(NetworkObserve())),
      ],
      child: const JenzCodingChallenge(),
    ),
  );
}

class JenzCodingChallenge extends StatelessWidget {
  const JenzCodingChallenge({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Jenz Coding Challenge',
      theme: JenzTheme.getThemeData(),
      home: const HomeScreen(),
    );
  }
}
