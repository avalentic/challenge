import 'package:flutter/material.dart';
import 'package:jenz/models/models.dart';

class GenreCard extends StatelessWidget {
  final Genre genre;

  const GenreCard(this.genre, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
      decoration: BoxDecoration(
        color: Theme.of(context).cardColor,
        borderRadius: const BorderRadius.all(Radius.circular(4)),
      ),
      child: Text(
        genre.name ?? "",
        style: Theme.of(context).textTheme.labelSmall,
      ),
    );
  }
}
