import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jenz/bloc/movie_bloc.dart';
import 'package:jenz/constants/endpoint_uris.dart';
import 'package:jenz/constants/jenz_icons.dart';
import 'package:jenz/models/models.dart';
import 'package:jenz/widgets/widgets.dart';

class MovieItem extends StatefulWidget {
  final Movie movie;
  final List<Genre> movieGenres;
  final bool isFavorite;
  final Function()? onTap;

  const MovieItem(this.movie, this.movieGenres, {this.isFavorite = false, this.onTap, Key? key}) : super(key: key);

  @override
  State<MovieItem> createState() => _MovieItemState();
}

class _MovieItemState extends State<MovieItem> {
  @override
  Widget build(BuildContext context) {
    // Didn't know how to break genres which don't have a space on screen, so I came up with the solution using Stack
    // There may be some better solutions, but I was in a hurry, so this is what I came with
    // I also made a first version of a widget just using Container/Row/Column widget, if you are interested you can
    // check that out from git history.
    return GestureDetector(
      onTap: widget.onTap != null ? () => widget.onTap!() : null,
      child: Stack(
        children: <Widget>[
          Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 100,
                  width: 100,
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(2)),
                  ),
                  child: Image.network(
                    EndpointUris.imagesHostName + (widget.movie.posterPath ?? ""),
                    frameBuilder: (BuildContext context, Widget child, int? frame, bool wasSynchronouslyLoaded) {
                      return child;
                    },
                    loadingBuilder: (BuildContext context, Widget child, ImageChunkEvent? loadingProgress) {
                      if (loadingProgress == null) {
                        return child;
                      } else {
                        return const Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                    },
                    errorBuilder: (BuildContext context, _, __) {
                      return Container(
                        color: Theme.of(context).cardColor,
                        child: Center(
                          child: SvgPicture.asset("assets/images/logo.svg"),
                        ),
                      );
                    },
                    fit: BoxFit.fill,
                  ),
                ),
                const SizedBox(width: 16),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(right: 32),
                        child: Text(
                          widget.movie.title ?? widget.movie.originalTitle ?? "",
                          style: Theme.of(context).textTheme.titleSmall,
                          overflow: TextOverflow.clip,
                        ),
                      ),
                      const SizedBox(height: 4),
                      RatingWidget(widget.movie.voteAverage ?? 0.0),
                      const SizedBox(height: 14),
                      _generateGenreCardWidgets(),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            top: 10,
            right: 5,
            child: GestureDetector(
              onTap: () => BlocProvider.of<MovieBloc>(context).add(AddOrRemoveFavoriteMovie(widget.movie)),
              child: Padding(
                padding: const EdgeInsets.only(left: 16, bottom: 16),
                child: Icon(
                  widget.isFavorite ? JenzIcons.favorite_checked : JenzIcons.favorite_unchecked,
                  size: 18,
                  color: widget.isFavorite ? Theme.of(context).primaryColor : const Color(0xFFE4ECEF),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _generateGenreCardWidgets() {
    List<Widget> children = <Widget>[];
    for (Genre genre in widget.movieGenres) {
      children.addAll(<Widget>[
        GenreCard(genre),
        const SizedBox(width: 4),
      ]);
    }
    return SingleChildScrollView(
      primary: false,
      scrollDirection: Axis.horizontal,
      child: Row(
        children: children,
      ),
    );
  }
}
