import 'package:flutter/material.dart';
import 'package:jenz/constants/jenz_icons.dart';
import 'package:jenz/constants/jenz_theme.dart';

class RatingWidget extends StatelessWidget {
  final double rating;
  final int maxRating;
  final String? suffixText;

  const RatingWidget(this.rating, {this.maxRating = 10, this.suffixText = "IMDb", Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        const Icon(
          JenzIcons.star,
          size: 13,
          color: JenzTheme.ratingStarIconColor,
        ),
        const SizedBox(width: 5),
        Text(
          "$rating / $maxRating ${suffixText ?? ""}",
          style: Theme.of(context).textTheme.labelMedium,
        ),
      ],
    );
  }
}
