import 'package:json_annotation/json_annotation.dart';

part 'spoken_language.g.dart';

@JsonSerializable()
class SpokenLanguage {
  @JsonKey(name: "iso_693_1")
  final String? isoCode;
  final String? name;

  factory SpokenLanguage.fromJson(Map<String, dynamic> json) => _$SpokenLanguageFromJson(json);

//<editor-fold desc="Data Methods">

  const SpokenLanguage({
    this.isoCode,
    this.name,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is SpokenLanguage && runtimeType == other.runtimeType && isoCode == other.isoCode && name == other.name);

  @override
  int get hashCode => isoCode.hashCode ^ name.hashCode;

  @override
  String toString() {
    return 'SpokenLanguage{' + ' isoCode: $isoCode,' + ' name: $name,' + '}';
  }

  SpokenLanguage copyWith({
    String? isoCode,
    String? name,
  }) {
    return SpokenLanguage(
      isoCode: isoCode ?? this.isoCode,
      name: name ?? this.name,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'isoCode': isoCode,
      'name': name,
    };
  }

  factory SpokenLanguage.fromMap(Map<String, dynamic> map) {
    return SpokenLanguage(
      isoCode: map['isoCode'] as String,
      name: map['name'] as String,
    );
  }

//</editor-fold>
}
