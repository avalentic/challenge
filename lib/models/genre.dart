import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';

part 'genre.g.dart';

@HiveType(typeId: 1)
@JsonSerializable()
class Genre extends HiveObject {
  @HiveField(0)
  final int? id;
  @HiveField(1)
  final String? name;

  factory Genre.fromJson(Map<String, dynamic> json) => _$GenreFromJson(json);

//<editor-fold desc="Data Methods">

  Genre({
    this.id,
    this.name,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Genre && runtimeType == other.runtimeType && id == other.id && name == other.name);

  @override
  int get hashCode => id.hashCode ^ name.hashCode;

  @override
  String toString() {
    return 'Genre{' + ' id: $id,' + ' name: $name,' + '}';
  }

  Genre copyWith({
    int? id,
    String? name,
  }) {
    return Genre(
      id: id ?? this.id,
      name: name ?? this.name,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'name': name,
    };
  }

  factory Genre.fromMap(Map<String, dynamic> map) {
    return Genre(
      id: map['id'] as int,
      name: map['name'] as String,
    );
  }

//</editor-fold>
}
