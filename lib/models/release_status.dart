import 'package:json_annotation/json_annotation.dart';

enum ReleaseStatus {
  @JsonValue("Rumored")
  RUMORED,
  @JsonValue("Planned")
  PLANNED,
  @JsonValue("In Production")
  IN_PRODUCTION,
  @JsonValue("Post Production")
  POST_PRODUCTION,
  @JsonValue("Released")
  RELEASED,
  @JsonValue("Canceled")
  CANCELED,
}
