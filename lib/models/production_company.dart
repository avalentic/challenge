import 'package:json_annotation/json_annotation.dart';

part 'production_company.g.dart';

@JsonSerializable()
class ProductionCompany {
  final int? id;
  @JsonKey(name: "logo_path")
  final String? logoPath;
  final String? name;
  @JsonKey(name: "origin_country")
  final String? originCountry;

  factory ProductionCompany.fromJson(Map<String, dynamic> json) => _$ProductionCompanyFromJson(json);

//<editor-fold desc="Data Methods">

  const ProductionCompany({
    this.id,
    this.logoPath,
    this.name,
    this.originCountry,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is ProductionCompany &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          logoPath == other.logoPath &&
          name == other.name &&
          originCountry == other.originCountry);

  @override
  int get hashCode => id.hashCode ^ logoPath.hashCode ^ name.hashCode ^ originCountry.hashCode;

  @override
  String toString() {
    return 'ProductionCompany{' +
        ' id: $id,' +
        ' logoPath: $logoPath,' +
        ' name: $name,' +
        ' originCountry: $originCountry,' +
        '}';
  }

  ProductionCompany copyWith({
    int? id,
    String? logoPath,
    String? name,
    String? originCountry,
  }) {
    return ProductionCompany(
      id: id ?? this.id,
      logoPath: logoPath ?? this.logoPath,
      name: name ?? this.name,
      originCountry: originCountry ?? this.originCountry,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'logoPath': logoPath,
      'name': name,
      'originCountry': originCountry,
    };
  }

  factory ProductionCompany.fromMap(Map<String, dynamic> map) {
    return ProductionCompany(
      id: map['id'] as int,
      logoPath: map['logoPath'] as String,
      name: map['name'] as String,
      originCountry: map['originCountry'] as String,
    );
  }

//</editor-fold>
}
