import 'package:jenz/models/genre.dart';
import 'package:jenz/models/production_company.dart';
import 'package:jenz/models/production_country.dart';
import 'package:jenz/models/release_status.dart';
import 'package:jenz/models/spoken_language.dart';
import 'package:json_annotation/json_annotation.dart';

part 'movie_details.g.dart';

@JsonSerializable()
class MovieDetails {
  final bool? adult;
  @JsonKey(name: "backdrop_path")
  final String? backdropPath;
  @JsonKey(name: "belongs_to_collection")
  final Object? belongsToCollection;
  final int? budget;
  final List<Genre>? genres;
  final String? homepage;
  final int? id;
  @JsonKey(name: "imdb_id")
  final String? imdbId;
  @JsonKey(name: "original_language")
  final String? originalLanguage;
  @JsonKey(name: "original_title")
  final String? originalTitle;
  final String? overview;
  final double? popularity;
  @JsonKey(name: "poster_path")
  final String? posterPath;
  @JsonKey(name: "production_companies")
  final List<ProductionCompany>? productionCompanies;
  @JsonKey(name: "production_countries")
  final List<ProductionCountry>? productionCountries;
  @JsonKey(name: "release_date")
  final DateTime? releaseDate;
  final int? revenue;
  final int? runtime;
  @JsonKey(name: "spoken_language")
  final List<SpokenLanguage>? spokenLanguages;
  final ReleaseStatus? status;
  final String? tagline;
  final String? title;
  final bool? video;
  @JsonKey(name: "vote_average")
  final double? voteAverage;
  @JsonKey(name: "vote_count")
  final int? voteCount;

  factory MovieDetails.fromJson(Map<String, dynamic> json) => _$MovieDetailsFromJson(json);

//<editor-fold desc="Data Methods">

  const MovieDetails({
    this.adult,
    this.backdropPath,
    this.belongsToCollection,
    this.budget,
    this.genres,
    this.homepage,
    this.id,
    this.imdbId,
    this.originalLanguage,
    this.originalTitle,
    this.overview,
    this.popularity,
    this.posterPath,
    this.productionCompanies,
    this.productionCountries,
    this.releaseDate,
    this.revenue,
    this.runtime,
    this.spokenLanguages,
    this.status,
    this.tagline,
    this.title,
    this.video,
    this.voteAverage,
    this.voteCount,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is MovieDetails &&
          runtimeType == other.runtimeType &&
          adult == other.adult &&
          backdropPath == other.backdropPath &&
          belongsToCollection == other.belongsToCollection &&
          budget == other.budget &&
          genres == other.genres &&
          homepage == other.homepage &&
          id == other.id &&
          imdbId == other.imdbId &&
          originalLanguage == other.originalLanguage &&
          originalTitle == other.originalTitle &&
          overview == other.overview &&
          popularity == other.popularity &&
          posterPath == other.posterPath &&
          productionCompanies == other.productionCompanies &&
          productionCountries == other.productionCountries &&
          releaseDate == other.releaseDate &&
          revenue == other.revenue &&
          runtime == other.runtime &&
          spokenLanguages == other.spokenLanguages &&
          status == other.status &&
          tagline == other.tagline &&
          title == other.title &&
          video == other.video &&
          voteAverage == other.voteAverage &&
          voteCount == other.voteCount);

  @override
  int get hashCode =>
      adult.hashCode ^
      backdropPath.hashCode ^
      belongsToCollection.hashCode ^
      budget.hashCode ^
      genres.hashCode ^
      homepage.hashCode ^
      id.hashCode ^
      imdbId.hashCode ^
      originalLanguage.hashCode ^
      originalTitle.hashCode ^
      overview.hashCode ^
      popularity.hashCode ^
      posterPath.hashCode ^
      productionCompanies.hashCode ^
      productionCountries.hashCode ^
      releaseDate.hashCode ^
      revenue.hashCode ^
      runtime.hashCode ^
      spokenLanguages.hashCode ^
      status.hashCode ^
      tagline.hashCode ^
      title.hashCode ^
      video.hashCode ^
      voteAverage.hashCode ^
      voteCount.hashCode;

  @override
  String toString() {
    return 'MovieDetails{' +
        ' adult: $adult,' +
        ' backdropPath: $backdropPath,' +
        ' belongsToCollection: $belongsToCollection,' +
        ' budget: $budget,' +
        ' genres: $genres,' +
        ' homepage: $homepage,' +
        ' id: $id,' +
        ' imdbId: $imdbId,' +
        ' originalLanguage: $originalLanguage,' +
        ' originalTitle: $originalTitle,' +
        ' overview: $overview,' +
        ' popularity: $popularity,' +
        ' posterPath: $posterPath,' +
        ' productionCompanies: $productionCompanies,' +
        ' productionCountries: $productionCountries,' +
        ' releaseDate: $releaseDate,' +
        ' revenue: $revenue,' +
        ' runtime: $runtime,' +
        ' spokenLanguages: $spokenLanguages,' +
        ' status: $status,' +
        ' tagline: $tagline,' +
        ' title: $title,' +
        ' video: $video,' +
        ' voteAverage: $voteAverage,' +
        ' voteCount: $voteCount,' +
        '}';
  }

  MovieDetails copyWith({
    bool? adult,
    String? backdropPath,
    Object? belongsToCollection,
    int? budget,
    List<Genre>? genres,
    String? homepage,
    int? id,
    String? imdbId,
    String? originalLanguage,
    String? originalTitle,
    String? overview,
    double? popularity,
    String? posterPath,
    List<ProductionCompany>? productionCompanies,
    List<ProductionCountry>? productionCountries,
    DateTime? releaseDate,
    int? revenue,
    int? runtime,
    List<SpokenLanguage>? spokenLanguages,
    ReleaseStatus? status,
    String? tagline,
    String? title,
    bool? video,
    double? voteAverage,
    int? voteCount,
  }) {
    return MovieDetails(
      adult: adult ?? this.adult,
      backdropPath: backdropPath ?? this.backdropPath,
      belongsToCollection: belongsToCollection ?? this.belongsToCollection,
      budget: budget ?? this.budget,
      genres: genres ?? this.genres,
      homepage: homepage ?? this.homepage,
      id: id ?? this.id,
      imdbId: imdbId ?? this.imdbId,
      originalLanguage: originalLanguage ?? this.originalLanguage,
      originalTitle: originalTitle ?? this.originalTitle,
      overview: overview ?? this.overview,
      popularity: popularity ?? this.popularity,
      posterPath: posterPath ?? this.posterPath,
      productionCompanies: productionCompanies ?? this.productionCompanies,
      productionCountries: productionCountries ?? this.productionCountries,
      releaseDate: releaseDate ?? this.releaseDate,
      revenue: revenue ?? this.revenue,
      runtime: runtime ?? this.runtime,
      spokenLanguages: spokenLanguages ?? this.spokenLanguages,
      status: status ?? this.status,
      tagline: tagline ?? this.tagline,
      title: title ?? this.title,
      video: video ?? this.video,
      voteAverage: voteAverage ?? this.voteAverage,
      voteCount: voteCount ?? this.voteCount,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'adult': adult,
      'backdropPath': backdropPath,
      'belongsToCollection': belongsToCollection,
      'budget': budget,
      'genres': genres,
      'homepage': homepage,
      'id': id,
      'imdbId': imdbId,
      'originalLanguage': originalLanguage,
      'originalTitle': originalTitle,
      'overview': overview,
      'popularity': popularity,
      'posterPath': posterPath,
      'productionCompanies': productionCompanies,
      'productionCountries': productionCountries,
      'releaseDate': releaseDate,
      'revenue': revenue,
      'runtime': runtime,
      'spokenLanguages': spokenLanguages,
      'status': status,
      'tagline': tagline,
      'title': title,
      'video': video,
      'voteAverage': voteAverage,
      'voteCount': voteCount,
    };
  }

  factory MovieDetails.fromMap(Map<String, dynamic> map) {
    return MovieDetails(
      adult: map['adult'] as bool,
      backdropPath: map['backdropPath'] as String,
      belongsToCollection: map['belongsToCollection'] as Object,
      budget: map['budget'] as int,
      genres: map['genres'] as List<Genre>,
      homepage: map['homepage'] as String,
      id: map['id'] as int,
      imdbId: map['imdbId'] as String,
      originalLanguage: map['originalLanguage'] as String,
      originalTitle: map['originalTitle'] as String,
      overview: map['overview'] as String,
      popularity: map['popularity'] as double,
      posterPath: map['posterPath'] as String,
      productionCompanies: map['productionCompanies'] as List<ProductionCompany>,
      productionCountries: map['productionCountries'] as List<ProductionCountry>,
      releaseDate: map['releaseDate'] as DateTime,
      revenue: map['revenue'] as int,
      runtime: map['runtime'] as int,
      spokenLanguages: map['spokenLanguages'] as List<SpokenLanguage>,
      status: map['status'] as ReleaseStatus,
      tagline: map['tagline'] as String,
      title: map['title'] as String,
      video: map['video'] as bool,
      voteAverage: map['voteAverage'] as double,
      voteCount: map['voteCount'] as int,
    );
  }

//</editor-fold>
}
