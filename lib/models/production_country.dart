import 'package:json_annotation/json_annotation.dart';

part 'production_country.g.dart';

@JsonSerializable()
class ProductionCountry {
  @JsonKey(name: "iso_3166_1")
  final String? isoCode;
  final String? name;

  factory ProductionCountry.fromJson(Map<String, dynamic> json) => _$ProductionCountryFromJson(json);

//<editor-fold desc="Data Methods">

  const ProductionCountry({
    this.isoCode,
    this.name,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is ProductionCountry &&
          runtimeType == other.runtimeType &&
          isoCode == other.isoCode &&
          name == other.name);

  @override
  int get hashCode => isoCode.hashCode ^ name.hashCode;

  @override
  String toString() {
    return 'ProductionCountry{' + ' isoCode: $isoCode,' + ' name: $name,' + '}';
  }

  ProductionCountry copyWith({
    String? isoCode,
    String? name,
  }) {
    return ProductionCountry(
      isoCode: isoCode ?? this.isoCode,
      name: name ?? this.name,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'isoCode': isoCode,
      'name': name,
    };
  }

  factory ProductionCountry.fromMap(Map<String, dynamic> map) {
    return ProductionCountry(
      isoCode: map['isoCode'] as String,
      name: map['name'] as String,
    );
  }

//</editor-fold>
}
