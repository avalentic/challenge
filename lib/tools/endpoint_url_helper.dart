class EndpointUrlHelper {
  static String getUrlWithParams(String url, Map<String, dynamic> params) {
    params.forEach((String key, dynamic value) {
      url = url.replaceAll("{" + key + "}", value);
    });
    return url;
  }
}
