import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:jenz/bloc/network_bloc.dart';

class NetworkHelper {
  static void observeNetwork() {
    Connectivity().onConnectivityChanged.listen((ConnectivityResult connectivityResult) {
      NetworkBloc().add(NetworkNotify(connectivityResult));
    });
  }
}
