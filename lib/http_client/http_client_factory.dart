import 'package:dio/dio.dart';
import 'package:dio/native_imp.dart';
import 'package:jenz/http_client/auth_http_interceptor.dart';

class HttpClientFactory {
  static const baseUrl = "https://api.themoviedb.org/3/";
  static final HttpClientFactory _instance = HttpClientFactory._internal();

  factory HttpClientFactory() {
    return _instance;
  }

  HttpClientFactory._internal();

  Dio client = _getClient();

  static Dio _getClient() {
    final DioForNative dio = DioForNative(BaseOptions(baseUrl: baseUrl));
    dio.interceptors.add(AuthHttpInterceptor());
    return dio;
  }
}
