import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class AuthHttpInterceptor extends InterceptorsWrapper {
  static const AUTHORIZATION_HEADER = "Authorization";

  @override
  Future<void> onRequest(RequestOptions options, RequestInterceptorHandler handler) async {
    _setAuthorizationHeader(options);
    return handler.next(options);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    return handler.next(response);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    return handler.next(err);
  }

  void _setAuthorizationHeader(RequestOptions options) {
    options.headers[AUTHORIZATION_HEADER] = "Bearer ${dotenv.env["BEARER_TOKEN"]}";
  }
}
